<?php

namespace Database\Factories;

use App\Models\UserUtil;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserUtilFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserUtil::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => rand(1, 20),
            'strong_color' => $this->faker->hexColor,
            'default_search_kind' => rand(0, 1) ? 'person' : 'jobs',
            'language' => rand(0, 1) ? 'esp' : 'eng'
        ];
    }
}
