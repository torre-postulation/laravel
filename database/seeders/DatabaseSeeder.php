<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserUtil;
use App\Models\Search;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(20)->create()->each(function ($user) {
            UserUtil::factory(20)->create([
                'user_id' => $user->id
            ]);
            Search::factory(30)->create([
                'user_id' => $user->id
            ]);
        });
    }
}
