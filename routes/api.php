<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/sign-up', [AuthController::class, 'register']);

Route::middleware(['auth:api'])->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/my-utils', [UserController::class, 'createUtils']);
    Route::get('/my-utils', [UserController::class, 'show']);
    Route::get('/torre-user/{user}', [UserController::class, 'getTorre']);
    Route::put('/my-utils', [UserController::class, 'update']);
    Route::get('/me', [UserController::class, 'me']);
    Route::get('/my-searchs', [SearchController::class, 'show']);
    Route::get('/last-searchs', [SearchController::class, 'lasts']);
    Route::post('/new', [SearchController::class, 'new_search']);
});
