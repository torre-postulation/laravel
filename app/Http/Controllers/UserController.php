<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function me(Request $request)
    {
        $user = User::with([
            'utils'
        ])->findOrFail($request->user()->id);

        return response()->json(['data' => $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::with([
            'searchs',
            'utils',
            'lasts_searchs' => function ($search) {
                return $search->orderBy('created_at', 'DESC')->limit(5);
            }
        ])
            ->withCount([
                'searchs',
                'searchs AS person_search' => function ($query) {
                    return $query->where('kind', 'person');
                },
                'searchs AS jobs_search' => function ($query) {
                    return $query->where('kind', 'job');
                }
            ])
            ->findOrFail($request->user()->id);

        return response()->json(['data' => $user]);
    }

    public function update(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'max:255',
            'email' => 'email|max:255',
            'password' => 'string|min:8|max:255',
            'strong_color' => 'string',
            'default_search_kind' => 'string',
            'language' => 'string',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => 'something went wrong',
                'validate' => $validation->errors()
            ], 400);
        }
        $user = User::find($request->user()->id);
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => 1,
        ]);
        $user->utils()->update([
            'strong_color' => $request->strong_color,
            'default_search_kind' => $request->default_search_kind,
            'language' => $request->language
        ]);
        return response()->json(['message' => 'Successfully updated profile']);
    }

    public function createUtils(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'strong_color' => 'required|string',
            'default_search_kind' => 'required|string',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => 'something went wrong',
                'validate' => $validation->errors()
            ]);
        }
        $user = User::find($request->user()->id);
        $user->utils()->create([
            'strong_color' => $request->strong_color,
            'default_search_kind' => $request->default_search_kind,
            'language' => 'eng'
        ]);
        $user = User::with([
            'utils'
        ])->findOrFail($request->user()->id);
        return response()->json(['data' => $user]);
    }

    public function getTorre($user)
    {
        $response = Http::retry(3, 150)->get("https://bio.torre.co/api/bios/${user}");
        return response()->json(['data' => $response->json()]);
    }
}
