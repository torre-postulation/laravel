<?php

namespace App\Http\Controllers;

use App\Models\Search;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user_search = User::with([
            'search',
            'search as lasts_searchs' => function ($search) {
                return $search->orderBy('created_at', 'DESC')->limit(5);
            }
        ])
            ->withCount(['search'])
            ->findOrFail($request->user()->id);

        return response()->json(['data' => $user_search]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function lasts(Request $request)
    {
        $last_searchs = User::with([
            'search' => function ($search) {
                return $search->orderBy('created_at', 'DESC')->limit(5);
            }
        ])
            ->findOrFail($request->user()->id);
        return response()->json(['data' => $last_searchs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function new_search(Request $request)
    {
        $user = User::findOrFail($request->user()->id);
        $user->searchs()->create([
            'search_string' => $request->search_string,
            'kind' => $request->kind
        ]);
        return response()->json([
            'data' => [
                'search_string' => $request->search_string,
                'kind' => $request->kind
            ]
        ]);
    }
}
