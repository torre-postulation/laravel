<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Register new user
     * 
     * @param string name
     * @param string lastname
     * @param string email
     * @param string password
     * @param string password_confirmation
     * @return string message
     */
    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:8|max:255',
            'password_confirmation' => 'same:password',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => 'something went wrong',
                'validate' => $validation->errors()
            ], 400);
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => 1,
        ]);
        $token = $user->createToken('Access Token')->accessToken;
        return response()->json([
            'message' => 'Successfully created user',
            'data' => [
                'user' => $user,
                'token' => $token
            ]
        ]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ])) {
            return response()->json([
                'message' => 'Error on login, try Again'
            ], 403);
        }

        $user = User::with('utils')->find($request->user()->id);
        $token = $user->createToken('Access Token')->accessToken;

        return response()->json([
            'message' => "succesfully loged in",
            'data' => [
                'user' => $user,
                'token' => $token,
            ],
        ], 200);
    }

    /**
     * Logout user (Revoke the Token)
     *
     * @return string message
     */
    public function logout()
    {
        $user = Auth::user()->token();
        $user->revoke();
        return response()->json(['message' => 'Logged out']);
    }
}
