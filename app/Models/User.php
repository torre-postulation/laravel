<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    const ROLE = [
        'user' => 1,
        1 => 'user',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // setting mutators to model 
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getRoleAttribute($value)
    {
        return self::ROLE[$this->attributes['role']];
    }

    // relations
    public function searchs()
    {
        return $this->hasMany(Search::class);
    }

    public function lasts_searchs()
    {
        return $this->hasMany(Search::class)->orderBy('created_at', 'DESC')->limit(5);
    }

    public function utils()
    {
        return $this->hasOne(UserUtil::class);
    }
}
