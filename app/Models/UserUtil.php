<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserUtil extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'strong_color',
        'default_search_kind',
        'language'
    ];

    // relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
