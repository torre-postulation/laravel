<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    use HasFactory;

    const KIND = [
        'person',
        'job'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'search_string',
        'kind'
    ];

    // relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
